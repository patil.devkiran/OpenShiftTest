#Usage: python app.py
import os
 
from flask import Flask, render_template, request, redirect, url_for
from werkzeug import secure_filename
#from keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
#from keras.models import Sequential, load_model
#import imutils
import time
import uuid
import base64
import model
import json


UPLOAD_FOLDER = 'upload'
ALLOWED_EXTENSIONS = set(['jpg', 'jpeg'])

application = Flask(__name__)
application.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@application.route('/', methods=['GET'])
def home():
    #return "Hello"
    return render_template('template.html', label='', imagesource='../upload/template.jpg')

# Serving Images
from flask import send_from_directory
@application.route('/upload/<filename>', methods=['GET'])
def uploaded_file(filename):
    return send_from_directory(application.config['UPLOAD_FOLDER'], filename)

@application.route('/', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        import time
        start_time = time.time()
        file = request.files['file']

        if file and allowed_file(file.filename):
            print("Received file")
            filename = secure_filename(file.filename)

            file_path = os.path.join(application.config['UPLOAD_FOLDER'], filename)
            file.save(file_path)
            result = model.predict(file_path)
           

            #label = {'img':filename, 'result': result}
            label = json.dumps(result)
            print(label)
            print(file_path)
            filename = my_random_string(6) + filename
            proctime = time.time() - start_time

            os.rename(file_path, os.path.join(application.config['UPLOAD_FOLDER'], filename))
            print("--- %s seconds ---" % str (proctime))
            return render_template('template.html', label=label, imagesource='../upload/' + filename, timetaken=proctime)
            #return jsonify(label)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

def my_random_string(string_length=10):
    """Returns a random string of length string_length."""
    random = str(uuid.uuid4()) # Convert UUID format to a Python string.
    random = random.upper() # Make all characters uppercase.
    random = random.replace("-","") # Remove the UUID '-'.
    return random[0:string_length] # Return the random string.

if __name__ == "__main__":
    application.debug=False
    application.run(host='0.0.0.0', port=3000)
